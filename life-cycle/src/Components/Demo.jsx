import React from "react";
import img1 from "../static/Images/100.jpg";
import img2 from "../static/Images/200.jpg";
class Demo extends React.Component {
  constructor(prop) {
    super(prop);
    this.state = { color: "red", image: img1 };
  }
  componentWillMount() {
    console.log("componentWillMount()");
  }

  componentDidMount() {
    console.log("componentDidMount()");
  }

  changeState = () => {
    this.setState({ color: "black!", image: img2 });
  };

  componentWillUnmount() {
    console.log("componenytwillUnmount()");
  }

  render() {
    const { color, image } = this.state;
    return (
      <div>
        <h1>hello muskan my fev color {color}</h1>
        <img src={image} alt={"mypicture"}></img>
        <h4>
          <a onClick={this.changeState}>click here</a>
        </h4>
      </div>
    );
  }
}
export default Demo;
