import React from "react";

function fun() {
  const Warnning = () => {
    alert(" Take care ");
  };

  return (
    <div>
      <h3> This is a Functional Component </h3>
      <button onClick={Warnning}> Alert </button>
    </div>
  );
}

export default fun;
