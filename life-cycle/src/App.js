import React from "react";
import "./App.css";
//import composing from "./Components/composing";
//import Lifecycle from "./Components/Lifecycle.jsx";
import Demo from "./Components/Demo.jsx";
import Func from "./Components/func.jsx";
function App() {
  return (
    <div className="App">
      <h1>React-Lifecycle</h1>

      <Demo />
      <Func />
    </div>
  );
}

export default App;
