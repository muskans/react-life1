import React from "react";
import "./App.css";
import Event from "./components/event.jsx";
import Func from "./components/func.jsx";
function App() {
  return (
    <div className="App">
      <h1>Hello Handler</h1>
      <Event />
      <Func />
    </div>
  );
}

export default App;
