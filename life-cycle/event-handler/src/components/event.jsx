import React, { Component } from "react";

class Event extends Component {
  constructor(props) {
    super(props);
    this.state = { name: " muskan ", age: 20 };
  }
  componentWillMount() {
    console.log("componentWillMount()");
  }

  componentDidMount() {
    console.log("componentDidMount()");
  }

  changeState = () => {
    this.setState({ name: " Mayra!", age: 21 });
  };

  render() {
    const { name, age } = this.state;
    return (
      <div>
        <h1>Hello Event Here!{name}</h1>
        <h2>My age is {age}</h2>
        <button>
          <a onClick={this.changeState}>click here</a>
        </button>
      </div>
    );
  }
}

export default Event;
