import React from "react";
function Eventhandle(props) {
  function changeClick() {
    console.log("Button event");
  }
  return (
    <div>
      <h1>Hello Function Event Here!!</h1>
      <button onClick={changeClick}>Click here</button>
    </div>
  );
}
export default Eventhandle;
